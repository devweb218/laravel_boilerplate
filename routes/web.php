<?php

use Illuminate\Support\Facades\Route;

Route::group(["namespace" => "App\Http\Controllers"], function (){

    Route::group(["prefix" => "ar", "as" => "ar."], function (){
        include("front/front.php");
        include("admin/admin.php");
    });
    Route::group(["prefix" => "fr", "as" => "fr."], function (){
        include("front/front.php");
        include("admin/admin.php");
    });
    include("front/front.php");
    include("admin/admin.php");

});

