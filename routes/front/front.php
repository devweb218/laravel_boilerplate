<?php

Route::get("lang/{lang?}", "BaseController@lang")->name("lang");

Route::group(["namespace" => "Front"], function () {

    Route::get("/", "Home@index")->name("index");
    Route::get("contact_us", "Home@contact_us")->name("contact_us");

    // Auth
    Route::group(["middleware" => "guest:users"], function () {

        Route::get("login", "AuthController@login")->name("login");
        Route::post("do_login", "AuthController@do_login")->name("do_login");
        Route::get("register", "AuthController@register")->name("register");
        Route::post("do_register", "AuthController@do_register")->name("do_register");
        Route::get('password/reset', 'AuthController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'AuthController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}/{email}', 'AuthController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'AuthController@reset')->name('password.update');

    });

    // Auth
    Route::group(["middleware" => "auth:users"], function () {

        Route::get("logout", "AuthController@logout")->name("logout");
        Route::get("my_account", "Profile@my_account")->name("my_account");
        Route::post("save_my_account", "Profile@save_my_account")->name("save_my_account");

        include("sites.php");

    });

});
