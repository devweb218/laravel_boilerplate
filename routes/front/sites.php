<?php

Route::group(["prefix" => "sites", "as" => "sites."], function () {

    Route::get("index", "Sites@index")->name("index");
    Route::get("add", "Sites@add")->name("add");
    Route::post("save", "Sites@save")->name("save");
    Route::get("edit/{id}", "Sites@edit")->name("edit");
    Route::post("update", "Sites@update")->name("update");
    Route::get("delete/{id}", "Sites@delete")->name("delete");
    Route::get("editor/{id}", "Sites@editor")->name("editor");

});
