<?php


Route::group(["namespace" => "Admin", "prefix" => "admin", "as" => "admin."], function () {

    // Auth
    Route::group(["middleware" => "guest:admins"], function () {

        Route::get("login", "AuthController@login")->name("login");
        Route::post("do_login", "AuthController@do_login")->name("do_login");

    });

    // Auth
    Route::group(["middleware" => "auth:admins"], function () {

        Route::get("/", "HomeController@index")->name("index");
        Route::get("logout", "AuthController@logout")->name("logout");

        // Admins
        Route::group(["prefix" => "admins", "as" => "admins."], function () {

            Route::get("/", "Admins@index")->name("index");
            Route::get("add", "Admins@add")->name("add");
            Route::post("save", "Admins@save")->name("save");
            Route::get("edit/{id?}", "Admins@edit")->name("edit");
            Route::post("update", "Admins@update")->name("update");
            Route::get("delete/{id?}", "Admins@delete")->name("delete");

        });

        // Users
        Route::group(["prefix" => "users", "as" => "users."], function () {

            Route::get("/", "Users@index")->name("index");
            Route::get("add", "Users@add")->name("add");
            Route::post("save", "Users@save")->name("save");
            Route::get("edit/{id?}", "Users@edit")->name("edit");
            Route::post("update", "Users@update")->name("update");
            Route::get("delete/{id?}", "Users@delete")->name("delete");

        });

        // Settings
        Route::group(["prefix" => "settings", "as" => "settings."], function () {

            Route::get("index", "Settings@index")->name("index");
            Route::post("save_settings", "Settings@save_settings")->name("save_settings");
            Route::post("save_smtp_settings", "Settings@save_smtp_settings")->name("save_smtp_settings");

        });

    });

});
