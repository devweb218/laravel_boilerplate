<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Inertia\Inertia;
use View;
use Session;
use Auth;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->setLocale($request);
            $this->share_vars();
            return $next($request);
        });
    }

    public function share_vars(){
        Inertia::share("lang", app()->getLocale());
        View::share("lang", app()->getLocale());
        Inertia::share('errors', (Session::get('errors')) ? Session::get('errors')->default->getMessages() : []);
        Inertia::share("message", Session::get('message'));
        Inertia::share("success", Session::get('success'));
        Inertia::share("error", Session::get('error'));
        Inertia::share("status", Session::get('status'));
        Inertia::share("resent", Session::get('resent'));
        $settings = Setting::get()->keyBy("key");
        View::share("settings", $settings);
        Inertia::share("_settings", $settings);
    }

    private function setLocale($request, $locale = null)
    {
        $from_lang_switcher = ($locale) ? true : false;
        if (!$locale) {
            $route = \Route::getCurrentRoute();
            if (isset($route->action["as"]))
                $as = explode(".", $route->action["as"]);
            else
                $as = ["en"];
            if (@$as[0] == "ar")
                $locale = "ar";
            elseif (@$as[0] == "fr")
                $locale = "fr";
            else
                $locale = "en";
        }
        if($from_lang_switcher){
            if(Auth::guard("users")->check())
                Auth::guard("users")->user()->setAttribute('locale', $locale)->save();
            if (Auth::guard("admins")->check())
                Auth::guard("admins")->user()->setAttribute('locale', $locale)->save();
        }
        $request->session()->put('locale', $locale);
        \App::setLocale($locale);
    }

    public function lang(Request $request, $lang = "en"){
        if (Auth::guard("users")->check())
            Auth::guard("users")->user()->setAttribute('locale', $lang)->save();
        if (Auth::guard("admins")->check())
            Auth::guard("admins")->user()->setAttribute('locale', $lang)->save();
        $request->session()->put('locale', $lang);
        \App::setLocale($lang);
        $url = $this->getUrlFromReferer($lang, $request);
        $url = $url ? $url : (($lang != "en") ? url('/' . $lang) : url('/'));

        return redirect($url);
    }

    private function getUrlFromReferer($locale, Request $request)
    {
        $url = $request->headers->get('referer');
        if(str_ends_with($url, "/ar"))
            $url = str_replace("/ar", "", $url);
        else
            $url = str_replace("ar/", "", $url);
        if(str_ends_with($url, "/fr"))
            $url = str_replace("/fr", "", $url);
        else
            $url = str_replace("fr/", "", $url);

        if ($locale != "en") {
            $url = str_replace(url("/"), '', $url);
            $url = rtrim(url("/"), '/') . '/' . $locale . '/' . ltrim($url, '/');
        }

        return $url;
    }

}
