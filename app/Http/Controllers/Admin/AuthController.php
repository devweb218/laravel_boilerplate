<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Admin;
use Auth;

class AuthController extends BaseAdminController
{
    use AuthenticatesUsers;

    public function __construct()
    {
        parent::__construct();
    }

    public function login()
    {
        return inertia('admin/auth/login');
    }

    public function do_login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    protected function attemptLogin(Request $request)
    {
        $remember = $request->remember and $request->remember == "1";
        return $this->guard()->attempt($this->credentials($request), $remember);
    }

    public function redirectPath()
    {
        return _route("admin.index");
    }

    protected function guard()
    {
        return Auth::guard("admins");
    }

    public function logout(Request $request)
    {
        $sessionKey = $this->guard()->getName();
        $this->guard()->logout();
        $request->session()->forget($sessionKey);
        $request->session()->regenerateToken();

        return redirect(_route("admin.login"));
    }

}
