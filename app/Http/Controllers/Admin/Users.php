<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;

class Users extends BaseAdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $users = User::orderby("id", "desc")->get();

        return inertia('admin/users/index',
            compact("users"));
    }

    public function add()
    {
        return inertia('admin/users/add',);
    }

    public function save(Request $request)
    {
        $request->validate(["name" => "required", "email" => "unique:users|required", "password" => "required"]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->status = $request->status;
        if ($request->hasFile("image"))
            $user->image = $request->image->store("uploads/users");
        $user->save();

        return redirect(_route("admin.users.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function edit($id = null)
    {
        $user = User::findOrFail($id);

        return inertia('admin/users/edit',
            compact("user"));
    }

    public function update(Request $request)
    {
        $request->validate(["name" => "required", "email" => "required|unique:users,email," . $request->id]);
        $user = User::findOrFail($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password and $request->password != "")
            $user->password = bcrypt($request->password);
        $user->status = $request->status;
        if ($request->hasFile("image")) {
            if ($user->image and file_exists(storage_path("app/" . $user->image)) and is_file(storage_path("app/" . $user->image)))
                unlink(storage_path("app/" . $user->image));
            $user->image = $request->image->store("uploads/users");
        }
        $user->save();

        return redirect(_route("admin.users.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function delete($id = null)
    {
        $user = User::findOrFail($id);
        $user->delete();
        if ($user->image and file_exists(storage_path("app/" . $user->image)) and is_file(storage_path("app/" . $user->image)))
            unlink(storage_path("app/" . $user->image));


        return redirect(_route("admin.users.index"))->with("success", __("l.Data Deleted Successfully"));
    }
}
