<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Inertia\Inertia;
use View;
use Auth;

class BaseAdminController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            if(Auth::guard("admins")->check())
                Inertia::share("auth_admin", Auth::guard("admins")->user());
            return $next($request);
        });
    }

}
