<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Admin;

class Admins extends BaseAdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $admins = Admin::orderby("id", "desc")->get();

        return inertia('admin/admins/index',
            compact("admins"));
    }

    public function add()
    {
        return inertia('admin/admins/add',);
    }

    public function save(Request $request)
    {
        $request->validate(["name" => "required", "email" => "unique:admins|required", "password" => "required"]);
        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->save();

        return redirect(_route("admin.admins.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function edit($id = null)
    {
        $admin = Admin::findOrFail($id);

        return inertia('admin/admins/edit',
            compact("admin"));
    }

    public function update(Request $request)
    {
        $request->validate(["name" => "required", "email" => "required|unique:admins,email," . $request->id]);
        $admin = Admin::findOrFail($request->id);
        $admin->name = $request->name;
        $admin->email = $request->email;
        if ($request->password and $request->password != "")
            $admin->password = bcrypt($request->password);
        $admin->save();

        return redirect(_route("admin.admins.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function delete($id = null)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();

        return redirect(_route("admin.admins.index"))->with("success", __("l.Data Deleted Successfully"));
    }
}
