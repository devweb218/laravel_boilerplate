<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Admin;

class Settings extends BaseAdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $settings = Setting::get()->keyBy("key");

        return inertia('admin/settings/index',
            compact("settings"));
    }

    public function save_settings(Request $request)
    {
        foreach ($request->all() as $k => $v) {
            if (in_array($k, ["logo"])) {
                if ($request->hasFile("logo")) {
                    $setting = Setting::where("key", $k)->first();
                    if ($setting)
                        if ($setting->value)
                            if (file_exists(storage_path("app/" . $setting->value)) and is_file(storage_path("app/" . $setting->value)))
                                unlink(storage_path("app/" . $setting->value));
                    $v = $request->{$k}->store("uploads");

                    $this->save_s($k, $v);
                }
            } else {
                $this->save_s($k, $v);
            }
        }

        return back()->with("success", __("l.Data Saved Successfully"));
    }

    protected function save_s($k, $v)
    {
        $setting = Setting::where("key", $k)->first();
        if (!$setting) {
            $setting = new Setting;
            $setting->key = $k;
        }
        $setting->value = $v;
        $setting->save();
    }

    public function save_smtp_settings(Request $request){
        foreach ($request->except("_token", "smtp_test_email") as $k => $v) {
            $this->save_s($k, $v);
        }
        if ($request->smtp_test_email != "") {
            $app_name = $request->email_sent_from_name;
            mailit($request->smtp_test_email, __("l.TEST MAIL"), __('l.This is a test mail from') . '<br> "' . $app_name . '"', null, $request->email_sent_from_address);
        }

        return back()->with("success", __("l.Data Saved Successfully"));
    }

}
