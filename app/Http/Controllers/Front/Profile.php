<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class Profile extends BaseFrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function my_account()
    {
        return inertia('front/profile/my_account');
    }

    public function save_my_account(Request $request){
        $user = User::findOrFail(auth()->guard("users")->user()->id);
        $request->validate(["name" => "required", "email" => "required|unique:users,email," . $user->id]);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password and $request->password != "")
            $user->password = bcrypt($request->password);
        if ($request->hasFile("image")) {
            $request->validate(["image" => "image"]);
            if ($user->image and file_exists(storage_path("app/" . $user->image)) and is_file(storage_path("app/" . $user->image)))
                unlink(storage_path("app/" . $user->image));
            $user->image = $request->image->store("uploads/users");
        }
        $user->save();

        return back()->with("success", __("l.Data Saved Successfully"));
    }

}
