<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Inertia\Inertia;

class Home extends BaseFrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return inertia('front/homepage');
    }

    public function contact_us()
    {
        return inertia('front/contact_us');
    }

}
