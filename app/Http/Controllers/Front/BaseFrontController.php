<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;

class BaseFrontController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            if(Auth::guard("users")->check())
                Inertia::share("auth", Auth::guard("users")->user());
            return $next($request);
        });
    }

}
