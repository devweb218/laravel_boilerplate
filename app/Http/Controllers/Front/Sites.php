<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Site;

class Sites extends BaseFrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $sites = Site::orderby("created_at", "asc")->get();

        return inertia('front/sites/index',
            compact("sites"));
    }

    public function add()
    {
        return inertia('front/sites/add');
    }

    public function save(Request $request)
    {
        $request->validate(["name" => "required"]);
        $site = new Site;
        $site->user_id = auth()->guard("users")->user()->id;
        $site->name = $request->name;
        if ($request->hasFile("image")) {
            $request->validate(["image" => "image"]);
            if ($site->image and file_exists(storage_path("app/" . $site->image)) and is_file(storage_path("app/" . $site->image)))
                unlink(storage_path("app/" . $site->image));
            $site->image = $request->image->store("uploads/sites");
        }
        $site->save();


        return redirect(_route("sites.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function delete($id = null)
    {
        $user_id = auth()->guard("users")->user()->id;
        $site = Site::where(["user_id" => $user_id, "id" => $id])->firstOrFail();
        if ($site->image and file_exists(storage_path("app/" . $site->image)) and is_file(storage_path("app/" . $site->image)))
            unlink(storage_path("app/" . $site->image));
        $site->delete();


        return redirect(_route("sites.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function edit($id = null)
    {
        $user_id = auth()->guard("users")->user()->id;
        $site = Site::where(["user_id" => $user_id, "id" => $id])->firstOrFail();

        return inertia('front/sites/edit',
            compact("site"));
    }

    public function update(Request $request)
    {
        $request->validate(["name" => "required"]);
        $user_id = auth()->guard("users")->user()->id;
        $site = Site::where(["user_id" => $user_id, "id" => $request->id])->firstOrFail();
        $site->name = $request->name;
        if ($request->hasFile("image")) {
            $request->validate(["image" => "image"]);
            if ($site->image and file_exists(storage_path("app/" . $site->image)) and is_file(storage_path("app/" . $site->image)))
                unlink(storage_path("app/" . $site->image));
            $site->image = $request->image->store("uploads/sites");
        }
        $site->save();

        return redirect(_route("sites.index"))->with("success", __("l.Data Saved Successfully"));
    }

    public function editor($id = null)
    {
        $user_id = auth()->guard("users")->user()->id;
        $site = Site::where(["user_id" => $user_id, "id" => $id])->firstOrFail();

        return inertia('front/sites/editor/index',
            compact("site"));
    }

}
