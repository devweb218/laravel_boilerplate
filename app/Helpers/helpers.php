<?php
use App\Mail\TestMail;
use App\Models\Setting;

if (!function_exists('all_lang')) {
    function all_lang()
    {
        $langs = [];
        $dir = 'resources/lang/';
        $files = array_diff(scandir($dir), array('..', '.'));
        foreach ($files as $x => $lang) {
            $langs[$lang] = [];
            $lang_files = array_diff(scandir($dir . $lang), array('..', '.'));
            foreach ($lang_files as $y => $file) {
                $f = str_replace(".php", "", $file);
                $langs[$lang][$f] = [];
                foreach (include($dir . $lang . "/" . $file) as $z => $v) {
                    $langs[$lang][$f][$z] = $v;
                }
            }
        }
        return $langs;
    }
}

if (!function_exists('_route')) {

    function _route($name, $parameters = [], $absolute = true)
    {
        $route = \Route::getCurrentRoute();
        if (isset($route->action["as"]))
            $as = explode(".", $route->action["as"]);
        else
            $as = ["en"];
        if (@$as[0] == "ar")
            $locale = "ar";
        elseif (@$as[0] == "fr")
            $locale = "fr";
        else
            $locale = "en";
        if ($locale != "en")
            $name = $locale . "." . $name;
        return app('url')->route($name, $parameters, $absolute);
    }
}

if (!function_exists('mailit')) {
    function mailit($to, $subject, $body, $attachments = null, $from = null)
    {
        try {
            init();
            Mail::to($to)->send(new TestMail($subject, $body, $attachments, $from));
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}

if (!function_exists('init')) {
    function init()
    {
        $csettings = Setting::all()->keyBy("key");
        $email_sent_from_address = @$csettings["email_sent_from_address"]->value;
        $email_sent_from_name = @$csettings["email_sent_from_name"]->value;;
        $email_smtp_host = @$csettings["smtp_host"]->value;
        $email_smtp_user = @$csettings["smtp_username"]->value;
        $email_smtp_password = @$csettings["smtp_password"]->value;
        $email_smtp_port = @$csettings["smtp_port"]->value;
        $encryption = @$csettings["smtp_security_type"]->value;
        Config::set('app.name', $email_sent_from_name);
        Config::set('mail.from.name', '"' . $email_sent_from_name . '"');
        Config::set('mail.from.address', $email_sent_from_address);

        if ($email_smtp_host and $email_smtp_user and $email_smtp_password and $email_smtp_port and $encryption) {
            Config::set('mail.mailers.smtp.host', $email_smtp_host);
            Config::set('mail.mailers.smtp.port', $email_smtp_port);
            Config::set('mail.mailers.smtp.username', $email_smtp_user);
            Config::set('mail.mailers.smtp.password', $email_smtp_password);
            Config::set('mail.mailers.smtp.encryption', $encryption);
        }
    }
}
