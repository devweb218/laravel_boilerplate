@php
    $app_name = \App\Models\Setting::where("key", "name_".app()->getLocale())->first();
    $app_name = ($app_name) ? $app_name->value : "Laravel Boilerplate";
    $logo = \App\Models\Setting::where("key", "logo")->first();
    $logo = ($logo) ? asset('public/' . $logo->value) : asset('public/noimage.jpg');
@endphp
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        a[x-apple-data-detectors] {color: inherit !important;}
        .thecontent, .thecontent *{
            @if(app()->getLocale()=="ar")
            text-align: right !important;
            direction: rtl !important;
            @else
            text-align: left !important;
            direction: rtl !important;
        @endif
            }
    </style>
</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 20px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
                <tr>
                    <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0;">
                        <img src="{{$logo}}" alt="{{$app_name}}" title="{{$app_name}}" width="80" height="80" style="display: block;" />
                        <h1 style="text-align: center; font-size: 35px !important; margin-bottom: 0px; color: #fff;">{{$app_name}}</h1>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                            <tr>
                                <td style="color: #153643; font-family: Arial, sans-serif;@if(app()->getLocale()=="ar") text-align: right !important; @else text-align: left !important; @endif">
                                    <h1 style="font-size: 24px; margin: 0;@if(app()->getLocale()=="ar") text-align: right !important; @else text-align: left !important; @endif">{{$subject}}</h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
                                    <p style="margin: 0;@if(app()->getLocale()=="ar") text-align: right !important; @else text-align: left !important; @endif">
                                        {!! $body !!}
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ee4c50" style="padding: 30px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                            <tr>
                                <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
                                    <p style="margin: 0; text-align: center">© {{ date('Y') }}. @lang('l.footer_copyrights')<br/>
                                        <a href="{{url("/")}}" style="color: #ffffff;">{{$app_name}}</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>
