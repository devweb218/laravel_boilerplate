@php
$admin = (strpos(request()->route()->getName(), "admin.") === 0 or strpos(request()->route()->getName(), "ar.admin.") === 0 or strpos(request()->route()->getName(), "fr.admin.") === 0) ? true: false
@endphp
<!DOCTYPE html>
<html lang="{{$lang}}" dir="{{($lang == 'ar') ? 'rtl' : 'ltr'}}">
<head>
    <title>{{@$settings["name_".app()->getLocale()]->value}}</title>
    <link rel="icon" type="image/png" href="{{(@$settings["logo"]) ? asset('public/' . @$settings["logo"]->value) : asset('public/noimage.jpg')}}"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet" media="all"/>
    <script src="{{ asset('public/js/app.js') }}" defer></script>
    @if($lang == "ar")
        <link rel="stylesheet" href="{{asset("public/bootstrap/css/bootstrap.rtl.min.css")}}" media="all"/>
    @else
        <link rel="stylesheet" href="{{asset("public/bootstrap/css/bootstrap.min.css")}}" media="all"/>
    @endif
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous" media="all"/>
    @if($admin)
        <link rel="stylesheet" href="{{asset("public/admin/adminlte.min.css")}}">
        @if($lang == "ar")
            <link rel="stylesheet" href="{{asset("public/admin/adminlte.min.rtl.css")}}">
        @endif
        <link href="{{ asset('public/admin/admin.css') }}" rel="stylesheet" media="all"/>
    @else
        <link href="{{ asset('public/front.css') }}" rel="stylesheet" media="all"/>
    @endif
    <link href="{{ asset('public/common.css') }}" rel="stylesheet" media="all"/>
    @routes
    <script>
        Ziggy.baseUrl = "{{url('/')."/"}}";
        var lang_to_vue = @json(all_lang());
        var lang = "{{$lang}}";
    </script>
</head>
<body class="@if(app()->getLocale() == "ar") rtl @endif @if($admin) hold-transition sidebar-mini layout-fixed layout-navbar-fixed @endif">
@inertia

@if($admin)
    <script src="{{ asset('node_modules/jquery/dist/jquery.js') }}"></script>
    <script>
        $(document).on("click", ".sidebar .nav-link", function (e) {
            console.log($(this).closest(".nav-item").find("ul.nav-treeview"))
            if($(this).closest(".nav-item").find("ul.nav-treeview").length > 0){
                e.preventDefault()
                $(this).closest(".nav-item").toggleClass("menu-open")
                $(this).closest(".nav-item").find("ul.nav-treeview").first().toggleClass("active")
            }
        })
        $(document).on("click", ".main-header .nav-link[data-widget='pushmenu']", function (e) {
            e.preventDefault()
            $("body").toggleClass("sidebar-collapse")
        })
    </script>
@endif
</body>
</html>
