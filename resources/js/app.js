import Vue, {methods, data} from "./bootstrap"
import {App, plugin} from '@inertiajs/inertia-vue'

// InertiaApp
Vue.use(plugin)

// Init App
const app = document.getElementById('app')
new Vue({
    render: h => h(App, {
        props: {
            initialPage: JSON.parse(app.dataset.page),
            resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),

        },
    }),
}).$mount(app)

// Mixin
Vue.mixin({methods: methods, data(){return data}})

