import Vue from 'vue'

// Jquery
window.$ = require('jquery')
window.JQuery = require('jquery')

// Bootstrap 5
import bootstrap from 'bootstrap/dist/js/bootstrap.bundle.min'

// InertiaProgress
import {InertiaProgress} from '@inertiajs/progress'
InertiaProgress.init({delay: 250, color: "#0d6efd",  showSpinner: true})

// VueMeta
import VueMeta from 'vue-meta'
Vue.use(VueMeta, {refreshOnceOnNavigation: true})

// Observable
var css_loaded = Vue.observable({css_loaded: false})
Object.defineProperty(Vue.prototype, '$css_loaded', {
    get() {
        return css_loaded.css_loaded
    },
    set(value) {
        css_loaded.css_loaded = value
    }
})

// Mixin Methods
export const methods = {
// routes
    route(...x) {
        try{
            var lg = this.$page.props.lang
            if (lg != "en" )
                x[0] = lg + "." + x[0];
            return route(...x)
        }catch (e){
            return route("index")
        }
    },
    // current route
    current: (x) => {
        if(lang != "en")
            x = lang + "." + x
        return  route().current(x)
    },
    // asset()
    asset: (x) => Ziggy.baseUrl + x,
    // translation
    translation(x) {
        var lg = this.$page.props.lang
        var file = x.split(".")[0]
        var key = x.split(".")[1]
        if (lang_to_vue[lg][file][key])
            return lang_to_vue[lg][file][key]
        else if (lang_to_vue["en"][file][key])
            return lang_to_vue["en"][file][key];
        return x;
    },
    // lang
    __(x) {
        return this.translation(x)
    },
    // toast
    toast: (type, text) => {
        let colors = {"success": "bg-success", "error": "bg-danger", "status": "bg-info", "message": "bg-info"}
        let div = document.createElement("div")
        div.innerHTML = `
                    <div class="toast d-flex align-items-center text-white ${colors[type]} border-0 mb-2" role="alert" aria-live="assertive" aria-atomic="true"
                    data-bs-autohide="true" data-bs-animation="true" data-bs-delay="400000">
                        <div class="toast-body">${text}</div>
                        <button type="button" class="btn-close btn-close-white ms-auto me-2" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>`
        document.getElementById("toasts").appendChild(div)
        new bootstrap.Toast(div.querySelector('.toast')).show()
        div.querySelector('.toast').addEventListener('hidden.bs.toast', function () {
            div.parentNode.removeChild(div)
        })
    },
    // Confirmation
    confirmation: () => {
        return new Promise(function (resolve, reject) {
            var confirmation_modal = new bootstrap.Modal(document.getElementById('confirmation-modal'))
            confirmation_modal.show()
            var result = false
            document.querySelector("#confirmation-modal .btn-yes").addEventListener('click', function () {
                confirmation_modal.hide()
                resolve(true)
            })
            document.querySelector("#confirmation-modal .btn-no").addEventListener('click', function () {
                confirmation_modal.hide()
                resolve(false)
            })
        }).then((result) => {
            return result
        })
    },
    async delete_confirm(e, callback) {
        let url = (e.target.href) || e.target.parentElement.href
        var component = this
        if (await this.confirmation()) {
            setTimeout(function () {
                if (callback) {
                    callback(e)
                } else
                    component.$inertia.visit(url, {preserveState: false, preserveScroll: true})
            }, 500)
        }
    },
}

// Mixin Data
export const data = {

}
export default Vue
