<?php

return array (
  'homepage' => 'Accueil',
  'contact_us' => 'Contactez Nous',
  'ar' => 'Arabic',
  'en' => 'English',
  'fr' => 'Frensh',
  'Data Saved Successfully' => 'Données sauvegardées avec succès',
  'Confirmation' => 'Confirmation',
  'Are you sure ?' => 'Êtes-vous sûr ?',
  'Yes' => 'Oui',
  'No' => 'Non',
);
