<?php

return array (
  'failed' => 'Ces informations d\'identification ne correspondent pas à nos archives.',
  'password' => 'Le mot de passe fourni est incorrect.',
  'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer en :seconds secondes.',
);
