<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;
use App\Models\Admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        if (!Admin::where("email", "admin@admin.com")->first()) {
            Admin::create([
                'name' => "Admin",
                'email' => "admin@admin.com",
                'password' => bcrypt("admin"),
            ]);
        }
        if (Setting::count() == 0) {
            Setting::create(["key" => "name_ar", "value" => "لارافيل بويلربلات"]);
            Setting::create(["key" => "name_en", "value" => "Laravel Boilerplate"]);
            Setting::create(["key" => "name_fr", "value" => "Laravel Boilerplate"]);
            Setting::create(["key" => "logo", "value" => "noimage.jpg"]);
            Setting::create(["key" => "email_sent_from_address", "value" => "laravel_boilerplate@google.com"]);
            Setting::create(["key" => "email_sent_from_name", "value" => "Laravel Boilerplate"]);
            Setting::create(["key" => "smtp_host", "value" => "smtp.mailtrap.io"]);
            Setting::create(["key" => "smtp_username", "value" => "fa0f2bd1f9713d"]);
            Setting::create(["key" => "smtp_password", "value" => "d21b6d8d607c54"]);
            Setting::create(["key" => "smtp_port", "value" => "2525"]);
            Setting::create(["key" => "smtp_security_type", "value" => "tls"]);
        }
    }
}
