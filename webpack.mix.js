const mix = require('laravel-mix');
const LiveReloadPlugin = require('webpack-livereload-plugin')

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .disableNotifications()
    .webpackConfig(webpack => {
        return {
            output: {
                publicPath: process.env.APP_URL + "public/",
                chunkFilename: 'js/[name].js?id=[chunkhash]',
            },
            plugins: [
                new LiveReloadPlugin(),
            ],
            resolve: {
                alias: {
                    'vue$': 'vue/dist/vue.common'
                }
            },
        };
    })
    .options({processCssUrls: false})
